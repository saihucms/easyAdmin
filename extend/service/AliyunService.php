<?php
/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 2018/5/19
 * Time: 20:42
 */

namespace service;


use Aliyun\Core\DefaultAcsClient;
use Aliyun\Core\Profile\DefaultProfile;
use think\facade\Env;
use Aliyun\Core\Config;
use Aliyun\Api\Sms\Request\V20170525\SendSmsRequest;
class AliyunService
{


    public static function sendsms($phone,$code,$temp){
       require_once Env::get('root_path') . 'vendor/aliyunsms/vendor/autoload.php';
       require_once Env::get('root_path').'vendor/aliyunsms/lib/Api/Sms/Request/V20170525/SendSmsRequest.php';
        //获取本地配置文件
        Config::load();
        $config = db('system_aliyun_sms')->find();
        $accessKeyId=$config['KeyId'];
        $accessKeySecret=$config['KeySecret'];
        $product = "Dysmsapi";
        //短信 API 产品域名
        $domain = "dysmsapi.aliyuncs.com";
        //暂时不支持多 Region
        $region = "cn-hangzhou";
        $endPointName = "cn-hangzhou";
        $profile = DefaultProfile::getProfile($region,$accessKeyId,$accessKeySecret);
        DefaultProfile::addEndpoint("cn-hangzhou", "cn-hangzhou", $product, $domain);
        $acsClient = new DefaultAcsClient($profile);
        $request = new SendSmsRequest();

        $request->setPhoneNumbers($phone);//必填-短信接收号码

        $request->setSignName($config['SignName']);//必填-短信签名: 如何添加签名可以参考阿里云短信或 TPshop 官方文档
        //必填-短信模板 Code
        $request->setTemplateCode($temp);//必填-短信签名: 如何添加签名可以参考阿里云短信或 TPshop 官方文档
        //选填-假如模板中存在变量需要替换则为必填(JSON 格式)
        $request->setTemplateParam("{\"code\":$code}");//短信签名内容:
        $resp = $acsClient->getAcsResponse($request);

        //短信发送成功返回 True，失败返回 false
        if ($resp && $resp->Code == 'OK') {
            return array('status' => 1, 'msg' => $resp->Code);
        } else {
            return array('status' => -1, 'msg' => $resp->Message . ' subcode:' . $resp->Code);
        }
    }

}