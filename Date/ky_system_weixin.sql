/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : easyadmin

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-05-19 20:07:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ky_system_weixin
-- ----------------------------
DROP TABLE IF EXISTS `ky_system_weixin`;
CREATE TABLE `ky_system_weixin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(45) DEFAULT NULL,
  `appid` varchar(45) DEFAULT NULL,
  `appsecret` varchar(32) DEFAULT NULL,
  `encodingaeskey` varchar(100) DEFAULT NULL,
  `mch_id` varchar(45) DEFAULT NULL,
  `partnerkey` varchar(45) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
