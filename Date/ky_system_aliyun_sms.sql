/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : easyadmin

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-05-19 22:02:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ky_system_aliyun_sms
-- ----------------------------
DROP TABLE IF EXISTS `ky_system_aliyun_sms`;
CREATE TABLE `ky_system_aliyun_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `KeyId` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KeySecret` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SignName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
